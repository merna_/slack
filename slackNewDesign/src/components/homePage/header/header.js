import React from "react";
import "./header.css";
export const Header = () => {
  return (
    <div className="homeHeader text-center">
      <br />
      <h1 className="">
        Slack makes it<span> downright pleasant</span> to work together
      </h1>
      <button className="try my-2 my-sm-0">TRY FOR FREE</button>

      <div className="slackMsg">
          <div className="topNav container">
              <div className="red"></div>
              <div className="yellow"></div>
              <div className="green"></div>
          </div>
          <div className="body">
              <div className="left">
                  
              </div>
          </div>
      </div>
    </div>
  );
};
