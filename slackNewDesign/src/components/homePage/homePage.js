import React from "react";
import { Navbar } from "../navbvar/navbar";
import {  Header} from "./header/header";
export const HomePage = () => {
  return (
    <>
      <Navbar />
      <Header/>
    </>
  );
};
