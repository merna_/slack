import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./websiteNavbar.css";

export class WebsiteNavbar extends Component {
  state = {
    nav: false,
    dropDownItems: [
      "Features",
      "Channels",
      "Integrations",
      "Security",
      "Slack Connect",
      "Solutions",
      "Customers",
    ],
  };
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }
  handleScroll = (nav) => {
    window.pageYOffset > 140 ? (nav = true) : (nav = false);
    this.setState({ nav });
  };
  render() {
    let { nav, dropDownItems } = this.state;
    return (
      <nav
        className={`navbar navbar-expand-lg   ${nav && "fixed"}`}
        id="navbar"
      >
        <div className="container">
          <Link className="navbar-brand" to="/home">
            <svg
              className="c-slacklogo--white svg-replaced"
              width="100"
              height="25"
              shapeRendering="geometricPrecision"
              aria-label="Slack"
              viewBox="0 0 240 60"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="m75.663 47.477 2.929-6.846c3.207 2.375 7.39 3.632 11.574 3.632 3.068 0 5.02-1.187 5.02-3.003-.07-5.03-18.477-1.118-18.617-13.764-.07-6.427 5.648-11.387 13.737-11.387 4.81 0 9.622 1.188 13.038 3.913l-2.737 6.992c-3.143-2.021-7.025-3.43-10.72-3.43-2.51 0-4.184 1.187-4.184 2.725.07 4.96 18.618 2.235 18.827 14.322 0 6.567-5.579 11.178-13.528 11.178-5.856 0-11.225-1.397-15.34-4.332m116.629-9.325a8.498 8.498 0 0 1 -7.405 4.33c-4.698 0-8.506-3.816-8.506-8.523s3.808-8.523 8.506-8.523a8.498 8.498 0 0 1 7.405 4.33l8.143-4.52c-3.05-5.451-8.868-9.137-15.548-9.137-9.839 0-17.815 7.991-17.815 17.85 0 9.858 7.976 17.85 17.815 17.85 6.68 0 12.498-3.686 15.548-9.137zm-82.477 12.958h10.18v-49.86h-10.179zm95.761-49.86v49.86h10.18v-14.938l12.063 14.938h13.012l-15.34-17.746 14.224-16.559h-12.454l-11.505 13.767v-29.322zm-54.218 15.557v4.053c-1.673-2.795-5.787-4.751-10.11-4.751-8.925 0-15.967 7.895-15.967 17.815s7.042 17.885 15.967 17.885c4.323 0 8.437-1.956 10.11-4.751v4.052h10.18v-34.303zm0 21.414c-1.464 2.445-4.532 4.26-7.948 4.26-4.699 0-8.507-3.815-8.507-8.522s3.808-8.523 8.507-8.523c3.416 0 6.484 1.886 7.948 4.4z" />
              <path
                d="m21.902.148c-3.299 0-5.973 2.68-5.973 5.985a5.979 5.979 0 0 0 5.973 5.985h5.974v-5.985a5.98 5.98 0 0 0 -5.974-5.985m0 15.96h-15.929c-3.299 0-5.973 2.68-5.973 5.986 0 3.305 2.674 5.985 5.973 5.985h15.93c3.298 0 5.973-2.68 5.973-5.985 0-3.306-2.675-5.986-5.974-5.986"
                fill="#36c5f0"
              />
              <path
                d="m59.734 22.094c0-3.306-2.675-5.986-5.974-5.986s-5.973 2.68-5.973 5.986v5.985h5.973a5.98 5.98 0 0 0 5.974-5.985m-15.929 0v-15.961a5.98 5.98 0 0 0-5.974-5.985c-3.299 0-5.973 2.68-5.973 5.985v15.96c0 3.307 2.674 5.987 5.973 5.987a5.98 5.98 0 0 0 5.974-5.985"
                fill="#2eb67d"
              />
              <path
                d="m37.831 60a5.98 5.98 0 0 0 5.974-5.985 5.98 5.98 0 0 0-5.974-5.985h-5.973v5.985c0 3.305 2.674 5.985 5.973 5.985m0-15.96h15.93c3.298 0 5.973-2.68 5.973-5.986a5.98 5.98 0 0 0-5.974-5.985h-15.929c-3.299 0-5.973 2.68-5.973 5.985a5.979 5.979 0 0 0 5.973 5.985"
                fill="#ecb22e"
              />
              <path
                d="m0 38.054a5.979 5.979 0 0 0 5.973 5.985 5.98 5.98 0 0 0 5.974-5.985v-5.985h-5.974c-3.299 0-5.973 2.68-5.973 5.985m15.929 0v15.96c0 3.306 2.674 5.986 5.973 5.986a5.98 5.98 0 0 0 5.974-5.985v-15.961a5.979 5.979 0 0 0-5.974-5.985c-3.299 0-5.973 2.68-5.973 5.985"
                fill="#e01e5a"
              />
            </svg>
          </Link>

          <svg
            onClick={this.props.openSidenav}
            xmlns="http://www.w3.org/2000/svg"
            width="18"
            height="12"
            viewBox="0 0 18 12"
            aria-label=""
            className="svg-replaced collapseBtn"
            shapeRendering="geometricPrecision"
          >
            <path d="M1.52 2C1.22667 2 0.983333 1.90667 0.79 1.72C0.596667 1.52667 0.5 1.28667 0.5 1C0.5 0.713334 0.596667 0.476667 0.79 0.290001C0.983333 0.0966671 1.22667 0 1.52 0H16.48C16.7733 0 17.0167 0.0966671 17.21 0.290001C17.4033 0.476667 17.5 0.713334 17.5 1C17.5 1.28667 17.4033 1.52667 17.21 1.72C17.0167 1.90667 16.7733 2 16.48 2H1.52ZM1.52 7C1.22667 7 0.983333 6.90667 0.79 6.72C0.596667 6.52667 0.5 6.28667 0.5 6C0.5 5.71333 0.596667 5.47667 0.79 5.29C0.983333 5.09667 1.22667 5 1.52 5H16.48C16.7733 5 17.0167 5.09667 17.21 5.29C17.4033 5.47667 17.5 5.71333 17.5 6C17.5 6.28667 17.4033 6.52667 17.21 6.72C17.0167 6.90667 16.7733 7 16.48 7H1.52ZM1.52 12C1.22667 12 0.983333 11.9067 0.79 11.72C0.596667 11.5267 0.5 11.2867 0.5 11C0.5 10.7133 0.596667 10.4767 0.79 10.29C0.983333 10.0967 1.22667 10 1.52 10H16.48C16.7733 10 17.0167 10.0967 17.21 10.29C17.4033 10.4767 17.5 10.7133 17.5 11C17.5 11.2867 17.4033 11.5267 17.21 11.72C17.0167 11.9067 16.7733 12 16.48 12H1.52Z"></path>
          </svg>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item dropdownHover">
                <Link className="nav-link" to="#" role="button">
                  Product
                </Link>
                <div className="productDropDown">
                  {dropDownItems.map((dropDownItem) => (
                    <Link className="dropdown-item" to=" " key={dropDownItem}>
                      {dropDownItem}
                    </Link>
                  ))}
                  <div className="dropdown-divider"></div>
                  <Link className="dropdown-item" to="">
                    <svg
                      className="nav-download__icon svg-replaced"
                      shapeRendering="geometricPrecision"
                      viewBox="0 0 19 16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="m3.7594 15.735c-0.68125 0-1.3094-0.1782-1.8844-0.5345-0.575-0.3564-1.0312-0.8368-1.3688-1.4412-0.3375-0.6045-0.50625-1.2631-0.50625-1.9757 0-0.649 0.14062-1.2567 0.42188-1.823 0.2875-0.5663 0.67812-1.0308 1.1719-1.3935-0.10625-0.37541-0.15937-0.75083-0.15937-1.1262 0-0.71265 0.16875-1.3712 0.50625-1.9757 0.3375-0.60448 0.79375-1.0817 1.3688-1.4317 0.575-0.35632 1.2-0.53448 1.875-0.53448 0.1 0 0.25 0.00636 0.45 0.01908 0.55625-0.94171 1.2969-1.6862 2.2219-2.2334 0.93125-0.54721 1.9312-0.82082 3-0.82082 1.1062 0 2.1312 0.2927 3.075 0.87808 0.9437 0.57902 1.6906 1.3648 2.2406 2.3575 0.5562 0.99261 0.8344 2.0743 0.8344 3.2451 0.55 0.4645 0.9781 1.034 1.2843 1.7084 0.3063 0.67447 0.4594 1.3903 0.4594 2.1475 0 0.8908-0.2125 1.7148-0.6375 2.472-0.4187 0.7572-0.9875 1.3553-1.7062 1.7943-0.7125 0.4454-1.4938 0.6682-2.3438 0.6682h-10.303zm10.303-1.4317c0.5937 0 1.1406-0.1559 1.6406-0.4677 0.5063-0.3181 0.9063-0.7444 1.2-1.2789 0.2938-0.5409 0.4407-1.1263 0.4407-1.7562 0-0.6363-0.1469-1.2217-0.4407-1.7562-0.2937-0.54085-0.6875-0.96717-1.1812-1.279-0.0563-0.03181-0.0969-0.06681-0.1219-0.10499-0.0187-0.03818-0.0281-0.08908-0.0281-0.15271 0-0.05726 0.0031-0.1368 0.0094-0.23861 0.0125-0.1018 0.0187-0.20361 0.0187-0.30542 0-0.91626-0.2125-1.7625-0.6375-2.5388s-1.0031-1.3903-1.7344-1.8421c-0.725-0.45813-1.5156-0.6872-2.3718-0.6872-0.65 0-1.2657 0.13362-1.8469 0.40087-0.58125 0.26087-1.0906 0.62992-1.5281 1.1071s-0.775 1.0212-1.0125 1.6321c-0.0375 0.10181-0.09375 0.15271-0.16875 0.15271-0.04375 0-0.0875-0.01273-0.13125-0.03818-0.30625-0.14634-0.63437-0.21952-0.98437-0.21952-0.425 0-0.81876 0.11135-1.1812 0.33405-0.35626 0.22271-0.64063 0.52813-0.85313 0.91626-0.20625 0.38178-0.30937 0.80173-0.30937 1.2599 0 0.30542 0.04687 0.59812 0.14062 0.87808 0.1 0.27361 0.2375 0.52176 0.4125 0.74446 0.04375 0.05091 0.06563 0.09863 0.06563 0.14317 0 0.05727-0.05313 0.09544-0.15938 0.11453-0.35625 0.06999-0.67812 0.2227-0.96562 0.45813-0.2875 0.23544-0.51563 0.52814-0.68438 0.87804-0.1625 0.35-0.24375 0.7254-0.24375 1.1263 0 0.4581 0.10313 0.8812 0.30938 1.2694 0.2125 0.3818 0.5 0.6872 0.86249 0.9162 0.3625 0.2227 0.75626 0.3341 1.1813 0.3341h10.303zm-4.4531-1.9089c-0.1875 0-0.35313-0.0732-0.49688-0.2195l-2.8125-2.8538c-0.1375-0.13998-0.20625-0.30542-0.20625-0.4963 0-0.19725 0.06875-0.36587 0.20625-0.50586 0.14375-0.14634 0.3125-0.21952 0.50625-0.21952 0.1875 0 0.35 0.06999 0.4875 0.20998l1.6125 1.6321v-3.9896c0-0.20361 0.06563-0.37223 0.19688-0.50585 0.1375-0.13999 0.30625-0.20998 0.50625-0.20998 0.18125 0 0.34687 0.06999 0.49692 0.20998 0.1375 0.12726 0.2062 0.29587 0.2062 0.50585v3.9896l1.6219-1.6321c0.1375-0.13999 0.3-0.20998 0.4875-0.20998 0.175 0 0.3469 0.07954 0.5156 0.23861 0.125 0.1209 0.1875 0.27997 0.1875 0.47722 0 0.20361-0.0687 0.37223-0.2062 0.50585l-2.8125 2.8538c-0.1563 0.1463-0.32192 0.2195-0.49692 0.2195z" />
                    </svg>
                    Download Slack
                  </Link>
                </div>
              </li>

              <li className="nav-item">
                <Link className="nav-link" to="">
                  Enterprise
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="">
                  Resurces
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="">
                  Pricing
                </Link>
              </li>
            </ul>
            <ul className="navbar-nav my-2 my-lg-0">
              <li className="nav-item">
                <Link className="nav-link signin" to="">
                  Sign in
                </Link>
              </li>
              <button className="talk my-2 my-sm-0">TALK TO SALES</button>
              <button className="try my-2 my-sm-0">TRY FOR FREE</button>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
