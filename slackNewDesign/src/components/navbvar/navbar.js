import React, { Component } from "react";
import { MobileNavbar } from "./mobileNavbar/mobileNavbar";
import { WebsiteNavbar } from "./websiteNavbar/websiteNavbar";

export class Navbar extends Component {
  state = {
    active: false,
  };
  openSidenav = () => {
    this.setState({ active: true });
  };
  closeSidenav = () => {
    this.setState({ active: false });
  };
  render() {
    return (
      <>
        <WebsiteNavbar openSidenav={this.openSidenav} />
        <MobileNavbar
          closeSidenav={this.closeSidenav}
          active={this.state.active}
        />
      </>
    );
  }
}
