import {HomePage} from '../components/homePage/homePage';

export const routes = [
    {
      path: "/home",
      exact: true,
      component:HomePage,
    } 
]
  