import React from "react";
import "./billingFrom.css";

const BillingFrom = () => {
  return (
    <div className="billingFrom">
      <div className="row">
        <ul>
          <li>
            <div className="billingTo">
              <h5>Billing From</h5>
              <p>Ryets Inc.</p>
              <p>John Doe</p>
              <p>332 South Wayside Rd,</p>
              <p>Houston, TX</p>
              <p>United States</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};
export default BillingFrom;
