import React from "react";
import "./invoiceTable.css";
import {withRouter} from 'react-router-dom';
class InvoiceTable extends React.Component {
 
  render() {
    return (
      <div className="invoicet">
        <div className="container-fluid">
          <div className="row ">
            <div className="col-md-5 col-sm-5 col-xs-12">
              <div className="car d-none d-sm-block">car</div>
            </div>
            <div className="col-md-5 col-sm-5 col-10">
              <div className="services d-none d-sm-block">Services</div>
            </div>
            <div className="col-md-2 col-sm-2 col-2">
              <div className="prices d-none d-sm-block">Price</div>
            </div>
          </div>
          {this.props.items.map((item) => (
            <div className="row border" >
              <div className="col-md-5 col-sm-5 col-xs-12">
                <div className="carData">
                  <div className="lines row">
                    <p className="line-c">{item.name}</p>
                   <a href="/report"className="copy"> <img src="Mask Group 2.svg"  alt="new Page" /></a>
                  </div>
                  <p className="fromTo">
                    {item.location.pickup}
                    <img src="Mask Group 1.svg" className="arrow" alt="arrow"/>
                    {item.location.delivery}
                  </p>
                </div>
              </div>
              <div className="col-md-5 col-sm-5 col-9">
                <div className="serviceData">
                  {item.services.map((ser) => (
                    <div>
                      {ser.name}
                      {ser.note == null ? null : (
                        <span className="has-tooltip">
                          <img src="Mask Group 5.svg" className="icon " alt="notes"/>
                          <p className="container">
                            <span className="tooltip tooltip-left ">
                              {ser.note}
                            </span>
                          </p>
                        </span>
                      )}
                    </div>
                  ))}
                </div>
              </div>
              <div className="col-md-2 col-sm-2 col-3">
                <div className="priceData">
                  {item.services.map((ser) => (
                    <p>${ser.price.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</p>
                  ))}
                </div>
              </div>
            </div>
          ))}
          <div className="row ">
            <div className="col-md-5 col-sm-5 col-xs-12"></div>
            <div className="col-md-5 col-sm-5 col-7 ">
              <div className="subtotal">Subtotal:</div>
            </div>
            <div className="col-md-2 col-sm-2 col-5 ">
              <div className="subtotalPrice ">$1,515.00</div>
            </div>
          </div>
          <div className="row ">
            <div className="col-md-5 col-sm-5 col-xs-12"></div>
            <div className="col-md-5 col-sm-5 col-7 border">
              <div className="fees">Fees:</div>
            </div>
            <div className="col-md-2 col-sm-2 col-5 border">
              <div className="feesPrice ">$31.51</div>
            </div>
          </div>
          <div className="row ">
            <div className="col-md-5 col-sm-5 col-xs-12"></div>
            <div className="col-md-5 col-sm-5 col-7 ">
              <div className="total">Total:</div>
            </div>
            <div className="col-md-2 col-sm-2 col-5 ">
              <div className="totalPrice ">$100,000.51</div>
            </div>
          </div>
          <div className="row ">
            <div className="col-md-5 col-sm-5 col-xs-12"></div>
            <div className="col-md-5 col-sm-5 col-9 "></div>
            <div className="col-md-2 col-sm-2 col-12 ">
              <div className="button ">
                <button>Pay</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(InvoiceTable)
