import React, { useEffect } from "react";
import Address from "./address/address";
import CarInfo from "./carInfo/carInfo";
import MapInfo from "./mapInfo/mapInfo";
import ReportNavbar from "./ReportNavbar/ReportNavbar";
import "./report.css";
import SliderImage from "./sliderImage/sliderImage";
import CarSlider from "./carSlider/carSlider";
import Hood from "./hood/hood";
import Front from "./front/front";
import Frt from "./frt/frt";
import Rear from "./rear/rear";
import Lh from "./lh/lh";
import Reflector from "./reflector/reflector";
import Tail from "./tail/tail";
import Mirror from "./mirror/mirror";
import Roof from "./roof/roof";
import Notes from "./notes/notes";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import $ from "jquery";
import { render } from "@testing-library/react";
import Diagram from "./diagram/diagram";
class Report extends React.Component {
  state = {
    items: [],
   };
  componentDidMount(){
    $('.hoodLink').on('click', function(e){
        e.preventDefault();
    
        var imgLink = $(this).children('img').attr('src');
    
        $('.mask').html('<div class="img-box"><img src="'+ imgLink +'"><a class="close">&times;</a>');
    
        $('.mask').addClass('is-visible fadein').on('animationend', function(){
          $(this).removeClass('fadein is-visible').addClass('is-visible');
        });
    
        $('.close').on('click', function(){
          $(this).parents('.mask').addClass('fadeout').on('animationend', function(){
            $(this).removeClass('fadeout is-visible')
          });
        });
    
      });
      window.scrollTo(0, 0);
      fetch("d.json")
        .then((response) => response.json())
        .then((result) => {
           this.setState({
             items:result,
            })
           

          });
}
  render() {
    const {items}=this.state
    return (
      <div className="report">
        <div className="mask"></div>

        <div className="container" style={{}}>
          <ToastContainer
            autoClose={.1}
            hideProgressBar={true}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            position="top"
            width="100%"
          />
          <div className="data">
            <ReportNavbar />

            <div className="paddings">
              <Address />
              <div className="border"></div>
              <CarInfo />
              <MapInfo />
              <br />
              <br />
              <div className="border"></div>
              <CarSlider />
              <div className="border"></div>
              <Diagram items={items}/>
              <div className="border"></div>
              <Hood />
              <div className="border"></div>
              <Front />
              <div className="border"></div>
              <Frt />
              <div className="border"></div>
              <Rear />
              <div className="border"></div>
              <Lh />
              <div className="border"></div>
              <Reflector />
              <div className="border"></div>
              <Tail />
              <div className="border"></div>
              <Mirror />
              <div className="border"></div>
              <Roof />
              <div className="border"></div>
              <Notes />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Report;
