import React, { useEffect } from 'react';
import './sliderImage.css';
import slick from 'slick-carousel/slick/slick.js';
import $ from 'jquery'
const SliderImage=()=>{
    useEffect(()=>{
$('#detail .main-img-slider').not('.slick-initialized').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    fade:true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 300,
    lazyLoad: 'ondemand', 
    asNavFor: '.thumb-nav',
    prevArrow: '<div class="slick-prev"><i class="i-prev"></i><span class="sr-only sr-only-focusable">Previous</span></div>',
    nextArrow: '<div class="slick-next"><i class="i-next"></i><span class="sr-only sr-only-focusable">Next</span></div>'
  });
  // Thumbnail/alternates slider for product page
  $('.thumb-nav').not('.slick-initialized').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    centerPadding: '0px',
    asNavFor: '.main-img-slider',
    dots: false,
    centerMode: false,
    draggable: true,
    speed:200,
    focusOnSelect: true,
    prevArrow: '<div class="slick-prev"><i class="i-prev"></i><span class="sr-only sr-only-focusable">Previous</span></div>',
    nextArrow: '<div class="slick-next"><i class="i-next"></i><span class="sr-only sr-only-focusable">Next</span></div>'  
  });
  
  
  //keeps thumbnails active when changing main image, via mouse/touch drag/swipe
  $('.main-img-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
    //remove all active class
    $('.thumb-nav .slick-slide').removeClass('slick-current');
    //set active class for current slide
    $('.thumb-nav .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');  
  });

    })
    return(
        <div>
<section id="detail">
  <div class="container-fluid">
    <div class="row">
     
          <div class="product-images demo-gallery">
            
            <div class="main-img-slider">
               <a data-fancybox="gallery" href="https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg"><img src="https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg" class="img-fluid"/></a>
               <a data-fancybox="gallery" href="https://restaurant-table-des-faubourgs.com/assets/components/tdf/js/fullPage-master/examples/imgs/bg2.jpg"><img src="https://restaurant-table-des-faubourgs.com/assets/components/tdf/js/fullPage-master/examples/imgs/bg2.jpg" class="img-fluid"/></a>
               <a data-fancybox="gallery" href="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634.jpg"><img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634.jpg" class="img-fluid"/></a>
               <a data-fancybox="gallery" href="https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg"><img src="https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg" class="img-fluid"/></a>
               <a data-fancybox="gallery" href="https://restaurant-table-des-faubourgs.com/assets/components/tdf/js/fullPage-master/examples/imgs/bg2.jpg"><img src="https://restaurant-table-des-faubourgs.com/assets/components/tdf/js/fullPage-master/examples/imgs/bg2.jpg" class="img-fluid"/></a>
               <a data-fancybox="gallery" href="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634.jpg"><img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634.jpg" class="img-fluid"/></a>
          	</div>
           
          <ul class="thumb-nav">
            <li><img src="https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg"/></li>
            <li><img src="https://restaurant-table-des-faubourgs.com/assets/components/tdf/js/fullPage-master/examples/imgs/bg2.jpg"/></li>
            <li><img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634.jpg"/></li>
            <li><img src="https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg"/></li>
            <li><img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634.jpg"/></li>
          </ul>
           
        </div>
         
     </div>
  </div>
</section>
        </div>
    )
}
export default SliderImage
