import React from "react";
import "./mapInfo.css";
import mapboxgl from "mapbox-gl/dist/mapbox-gl.js"; 
mapboxgl.accessToken =
  "pk.eyJ1IjoibWVybmEtZXp6MjgiLCJhIjoiY2tkenRwNTc5MzliZDJzb2QzenhudWx0eCJ9.GUBl5uOpfVc8FNFBudHjiA ";

class MapInfo extends React.Component {
  state = {
    pickup: -79.51016257714582,
    pickup2: 43.702399671175385,
  };
  componentDidMount() {
    var map = new mapboxgl.Map({
      container: "map",
      style: "mapbox://styles/merna-ezz28/ckia7b400b13k19lrl5bifocf",
      center: [-79.4512, 43.6568], // starting position
      zoom: 12.7,
    });
  
    map.flyTo({
      center: [-79.58486038106673,43.576508365604354],
      essential: true,// this animation is considered essential with respect to prefers-reduced-motion
    
      bounds:[
        [-79.57202033691038,43.56536806783515],
        [-79.58486038106673,43.576508365604354]
      ],
      pitch:false,
      bearing:false
    
    });
    
    var start = [-79.57202033691038,43.56536806783515];

    var canvas = map.getCanvasContainer();
    const getRoute = (end) => {
      // make a directions request using cycling profile
      // an arbitrary start will always be the same
      // only the end or destination will change
      var start = [-79.57202033691038,43.56536806783515];
      // driving, driving-traffic
      var url =
        "https://api.mapbox.com/directions/v5/mapbox/driving-traffic/" +
        start[0] +
        "," +
        start[1] +
        ";" +
        end[0] +
        "," +
        end[1] +
        "?steps=true&geometries=geojson&access_token=" +
        mapboxgl.accessToken;

      // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
      var req = new XMLHttpRequest();
      
      req.open("GET", url, true);
      req.onload = function () {
        var json = JSON.parse(req.response);
         document.getElementById('distances').innerHTML=(((json.routes[0].distance)/1.609)/1000).toFixed(2)
        var data = json.routes[0];
        var route = data.geometry.coordinates;
 
        var geojson = {
          type: "Feature",
          properties: {},
          geometry: {
            type: "LineString",
            coordinates: route,
          },
        };
        // if the route already exists on the map, reset it using setData
        if (map.getSource("route")) {
          map.getSource("route").setData(geojson);
        } else {
          // otherwise, make a new request
          map.addLayer({
            id: "route",
            type: "line",
            source: {
              type: "geojson",
              data: {
                type: "Feature",
                properties: {},
                geometry: {
                  type: "LineString",
                  coordinates: geojson,
                },
              },
            },
            layout: {
              "line-join": "round",
              "line-cap": "round",
            },
            paint: {
              "line-color": "#0078D4",
              "line-width": 1,
              "line-opacity": 0.75,
            },
          });
        }
        // add turn instructions here at the end
      };
      req.send();
    };

    map.on("load", function () {
      // make an initial directions request that
      // starts and ends at the same location
      getRoute(start);
      map.loadImage("Group 166@2x.png", function (error, image) {
        if (error) throw error;
        map.addImage("cat", image);
        map.addSource("point", {
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: [
              {
                type: "Feature",
                geometry: {
                  type: "Point",
                  coordinates: start,
                },
              },
            ],
          },
        });
        map.addLayer({
          id: "routes",
          type: "symbol",
          source: "point",
          layout: {
            "icon-image": "cat",
            "icon-size": 0.45,
          },
        });
      });
    });
    // this is where the code from the next step will go
    map.on("load", function () {
      var coords = [-79.58486038106673,43.576508365604354];
      var end = {
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            properties: {},
            geometry: {
              type: "Point",
              coordinates: coords,
            },
          },
        ],
      };
      if (map.getLayer("end")) {
        map.getSource("end").setData(end);
      } else {
        map.loadImage("Group 166@2x.png", function (error, image2) {
          if (error) throw error;
          map.addImage("cat2", image2);
          map.addSource("point2", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: coords,
                  },
                },
              ],
            },
          });
          map.addLayer({
            id: "end",
            type: "symbol",
            source: "point2",
            layout: {
              "icon-image": "cat2",
              "icon-size": 0.45,
            },
          });
        });
      }
      getRoute(coords);
     
     
    }); 
  }
  render() {
    const { totalReactPackages } = this.state;

    return (
      <div className="map">
        <div id="map" class="mapboxgl-map mapInfo">
          {/* <div className="mapFromTo container">
            <ul>
              <li><div className="from">
                <p>AutoDeal, inc</p>
                <span>Austin, TX</span>
              </div></li>
              <li><img className="arrow" src="Mask Group 6.svg" /></li>
              <li>
                <div className="to">
                  <p>West Port</p>
                  <span>Houston, TX</span>
                </div></li>
              <li className="distanceRight">
                <div className="distance row">
                  <img src="Mask Group 7.svg" /><span>2,150 mi</span>
                </div></li>
            </ul>
          </div> */}
          <div className="container mapData">
            <div className="row">
              <div className="col-md-8 col-sm-8 col-12">
                <div className="col-md-12" style={{ display: "flex" }}>
                  <div className="col-md-5 col-sm-5">
                    <div className="from">
                    <div>AutoDeal, inc</div>
                    <div>Austin, TX</div>
                    </div>
                  </div>
                  <div className="col-md-2 col-sm-2">
                    <img className="img1" src="Mask Group 6.svg"/>
                  </div>
                  <div className="col-md-5 col-sm-5">
                  <div className="to">
                    <div>West Port</div>
                    <div>Houston, TX</div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="col-md-4 col-sm-4 col-12"
              >
                <div className="distance row">
                  <p ><img src="Mask Group 7.svg"/>
                 <span id="distances"> </span> mi</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MapInfo;
